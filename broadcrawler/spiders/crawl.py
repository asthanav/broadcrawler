# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import CrawlSpider, Rule, SitemapSpider
from scrapy.linkextractors import LinkExtractor
from broadcrawler.items import BroadcrawlerItem


class CrawlSpider(SitemapSpider):
    name = "crawl"

    def start_requests(self):
        requests = list(super(CrawlSpider, self).start_requests())
        requests += [scrapy.Request('http://{}'.format(self.domain), meta={'source': 'root'})]
        return requests

    def parse(self, response):
        item = BroadcrawlerItem()
        domain = '.'.join(response.url.split('/')[2].split('.')[-2:])
        if domain != self.domain:
            item['type'] = 'external'
        else:
            item['type'] = 'internal'
            if response.__class__ is scrapy.http.response.html.HtmlResponse:
                for link in response.xpath('//a[@href]/@href').extract():
                    link = response.urljoin(link)
                    if link.find('http') > -1:
                        yield scrapy.Request(response.urljoin(link), meta={'source': response.url})
        item['url'] = response.url
        item['status'] = response.status
        item['source'] = response.meta.get('source', 'sitemap')
        if response.status != 200:
            if item['type'] == 'external':
                self.links[1].append(item)
            else:
                self.links[0].append(item)
        return
